Rails.application.routes.draw do
  namespace :user do
  get 'oauths/oauth'
  end

  namespace :user do
  get 'oauths/callback'
  end

  resources :challenges

  namespace :users, as: :user do
    resource :registration, only: [:new, :create]
    resource :session, only: [:new, :create, :destroy]
    get :activate, controller: 'registrations', action: 'activate'

    post "oauth/callback" => "oauths#callback"
    get "oauth/callback" => "oauths#callback"
    get "oauth/:provider" => "oauths#oauth", :as => :auth_at_provider

    resource :profile, only: [:show] do
      get :edit, on: :collection
      patch :update, on: :collection
    end
  end

  delete 'logout' => 'users/sessions#destroy', :as => :logout
  get 'challenge' => 'home#challenge', as: :current_challenge

  root to: 'home#index'
end

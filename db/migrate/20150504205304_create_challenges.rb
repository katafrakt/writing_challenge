class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.text :title
      t.string :content
      t.string :words, array: true, null: false
      t.integer :target_word_count, null: false
      t.integer :word_count
      t.integer :time_limit, null: false
      t.integer :user_id, null: false
      t.datetime :started_at, null: false

      t.timestamps null: false
    end
  end
end

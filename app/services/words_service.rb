class WordsService
	def initialize
		load_list
	end

	def load_list
		@list = YAML.load_file(Rails.root.join('config', 'words.yml'))
	end

	def sample(n=3)
		@list.sample(n)
	end
end
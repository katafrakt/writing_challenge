class ChallengeLevelService
	WrongName = Class.new(StandardError)

	def initialize(name)
		@name = name
	end

	def call
		begin
			send(@name)
		rescue
			raise WrongName
		end
	end

	private

	def get_words(n)
		word_service.sample(n)
	end

	def word_service
		@word_service ||= WordsService.new
	end

	# defining levels as separate methods
	# smart? maybe not
	# but what if there is going to be some more complex logic
	# than just choosing time, word count and words?

	def express
		{
			target_word_count:  150,
			words: get_words(3),
			time_limit: 2
		}
	end

	def normal
		{
			target_word_count: 250,
			words: get_words(3),
			time_limit: 15
		}
	end

	def storyteller
		{
			target_word_count: 250,
			words: get_words(5),
			time_limit: 15
		}
	end

	def skald
		{
			target_word_count: 300,
			words: get_words(5),
			time_limit: 15
		}
	end
end
class HomeController < ApplicationController
	def index
		@latest_challenges = Challenge.completed.order(started_at: :desc).limit(10).includes(:user)
	end

	def challenge
		@challenge = current_user.current_challenge
		redirect_to new_challenge_path if @challenge.nil?
	end
end

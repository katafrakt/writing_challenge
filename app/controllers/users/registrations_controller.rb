module Users
	class RegistrationsController < ApplicationController
		def new
			@user = User.new
		end

		def create
			@user = User.new(safe_params)
			@user.save!
			@user.activate! # for now
			redirect_to :root
		end

		def activate
			if (@user = User.load_from_activation_token(params[:token]))
				@user.activate!
				redirect_to(new_user_session_path, :notice => 'User was successfully activated.')
			else
				not_authenticated
			end
		end

		private
		def safe_params
			params.require(:user).permit(:name, :email, :password, :password_confirmation)
		end
	end
end
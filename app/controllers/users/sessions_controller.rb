module Users
	class SessionsController < ApplicationController
		def new
			@user = User.new
			redirect_to :root if logged_in?
		end

		def create
			if @user = login(params[:user][:email], params[:user][:password])
				redirect_back_or_to(:root, notice: 'Login successful')
			else
				@user = User.new
				flash.now[:alert] = 'Login failed'
				redirect_to action: 'new'
			end
		end

		def destroy
			logout
			redirect_to(:root, notice: 'Logged out!')
		end
	end
end
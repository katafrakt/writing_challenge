module Users
	class ProfilesController < ApplicationController
		def edit
			@user = current_user
		end

		def update
			@user = current_user
			passwords = strip_password_params
			if passwords.length > 0
				unless User.authenticate(@user.email, passwords[:old])
					flash[:error] = 'Niepoprawne hasło'
					render action: :edit and return
				end

				@user.password = passwords[:new]
				@user.password_confirmation = passwords[:confirmation]
			end

			@user.name = safe_params[:name]

			if @user.save
				redirect_to :root
			else
				render :edit
			end
		end

		private
		def safe_params
			params.require(:user).permit(:name)
		end

		def strip_password_params
			{
				old: params[:user].delete(:old_password),
				new: params[:user].delete(:password),
				confirmation: params[:user].delete(:password_confirmation)
			}.reject{|k,v| v.blank?}
		end
	end
end
class ChallengesController < ApplicationController
	def new
		redirect_to current_challenge_path if current_user.current_challenge

		@challenge_levels = [:express, :normal, :storyteller, :skald].map do |level|
			level_params = ChallengeLevelService.new(level).call
			level_params[:words] = level_params[:words].length
			level_params[:name] = t('levels.' + level.to_s)
			level_params[:internal_name] = level

			ChallengeLevel.new.tap do |cl|
				level_params.each {|k,v| cl.send("#{k}=", v)}
			end
		end
	end

	def index
		@challenges = Challenge.completed
	end

	def create
		redirect_to current_challenge_path if current_user.current_challenge
		
		# TODO: add error handling
		parameters = ChallengeLevelService.new(params[:challenge_level][:internal_name]).call
		@challenge = current_user.challenges.create!(parameters)
		redirect_to current_challenge_path
	end

	def update
		@challenge = Challenge.find(params[:id])
		if @challenge.update_attributes!(safe_params)
			render json: {url: challenge_path(@challenge)}
		else
			render @challenge.errors.to_json, status: 422
		end
	end

	def show
		@challenge = Challenge.find(params[:id])
	end

	private
	def safe_params
		params.require(:challenge).permit(:content)
	end
end

class User < ActiveRecord::Base
	attr_accessor :old_password
	
	authenticates_with_sorcery! do |config|
		config.authentications_class = Authentication
	end

	has_many :authentications, :dependent => :destroy
	accepts_nested_attributes_for :authentications

	validates :password, confirmation: true

	has_many :challenges

	def current_challenge
		challenges.where(["started_at + (time_limit * interval '1 minutes') > ?", Time.now]).first
	end
end

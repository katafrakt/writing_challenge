class Challenge < ActiveRecord::Base
	belongs_to :user

	scope :completed, -> { where.not(content: nil) }

	before_save on: :create do
		self.started_at = Time.now
	end

	def display_title
		title || words.join(", ")
	end
end
